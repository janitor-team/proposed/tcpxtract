tcpxtract (1.0.1-17) unstable; urgency=medium

  * debian/control: bumped Standards-Version to 4.6.2.
  * debian/copyright: updated packaging copyright years.
  * debian/upstream/metadata: added 2 new fields.

 -- Thiago Andrade Marques <andrade@debian.org>  Mon, 06 Feb 2023 11:32:37 -0300

tcpxtract (1.0.1-16) unstable; urgency=medium

  * debian/control: updated to Standards-Version 4.6.0.1.
  * debian/copyright: updated packaging copyright years.

 -- Thiago Andrade Marques <andrade@debian.org>  Wed, 27 Oct 2021 14:05:15 -0300

tcpxtract (1.0.1-15) unstable; urgency=medium

  * debian/control:
      - Added byacc in Build-Depends field.
      - Bumped DH level to 13.
      - Updated my email in maintainer field.
  * debian/copyright: Updated my email address.
  * debian/patches/{50_fix-spelling-binary.patch, 60_libfl.patch}: forwarded
      patches upstream.
  * debian/upstream/metadata: created.

 -- Thiago Andrade Marques <andrade@debian.org>  Tue, 15 Sep 2020 08:29:04 -0300

tcpxtract (1.0.1-14) unstable; urgency=medium

  * New maintainer. Thanks to Joao Eriberto Mota Filho, the last maintainer.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.5.0.
  * debian/copyright: added packaging rights for me.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.

 -- Thiago Andrade Marques <thmarques@gmail.com>  Wed, 25 Mar 2020 08:17:22 -0300

tcpxtract (1.0.1-13) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * debian/copyright: added rights for Ondřej Nový.
  * debian/tests/control: using a Restriction field instead of '2>'.

  [ Ondřej Nový ]
  * debian/watch: use https protocol.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 04 Dec 2018 10:16:44 -0200

tcpxtract (1.0.1-12) unstable; urgency=medium

  * Ack for NMU. Thanks to Adrian Bunk. (Closes: #891523)
  * Migrated DH level to 11.
  * debian/control:
      - Bumped Standards-Version to 4.2.1.
      - Changed Vcs-* URLs to salsa.debian.org.
      - Removed no longer needed build dependency dh-autoreconf.
  * debian/copyright:
      - Added rights for Adrian Bunk.
      - Updated packaging copyright years.
      - Using a secure copyright format in URI.
  * debian/source/include-binaries: added to include debian/tests/test.pcap.
  * debian/tests/*: added to perform tests.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 08 Sep 2018 16:28:34 -0300

tcpxtract (1.0.1-11.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with shared libfl. (Closes: #891523)

 -- Adrian Bunk <bunk@debian.org>  Tue, 10 Apr 2018 22:35:11 +0300

tcpxtract (1.0.1-11) unstable; urgency=medium

  * debian/control: added libfl-dev to Build-Depends field.
    Thanks to Helmut Grohne <helmut@subdivi.de>. (Closes: #846451)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 01 Dec 2016 11:17:16 -0200

tcpxtract (1.0.1-10) unstable; urgency=medium

  * Bumped DH level to 10.
  * debian/control:
      - Bumped Standards-Version to 3.9.8.
      - Updated the Vcs-* fields to use https instead of http and git.
  * debian/copyright: updated the packaging copyright years.
  * debian/patches/50_fix-spelling-binary.patch: added to fix a spelling
    error in final binary.
  * debian/watch: bumped to version 4.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 20 Nov 2016 19:42:52 -0200

tcpxtract (1.0.1-9) unstable; urgency=medium

  * New maintainer. Thanks to Nico Golde for all work around this package.
      (Closes: #800754)
  * Migrations:
      - debian/copyright to 1.0 format.
      - debian/rules to new (reduced) format.
      - DH level to 9.
      - Using dh-autoreconf now. (Closes: #727984)
  * debian/control:
      - Added the Vcs-* fields.
      - Bumped Standards-Version to 3.9.6.
      - Improved the long description.
      - Removed quilt from Build-Depends field.
  * debian/copyright: updated all information.
  * debian/patches:
      - Renamed:
          ~ 01_fixmanpage.patch to 10_fixmanpage.patch.
          ~ tcpxtract-fix-segfault.patch to 20_tcpxtract-fix-segfault.patch.
          ~ fix-excessive-sync.patch to 30_fix-excessive-sync.patch.
          ~ 02-fix_png_header_bytes.patch to 40_fix_png_header_bytes.patch.
      - Added the 'Last-Update' field to 10_fixmanpage.patch.
      - Remade the header of the 20_tcpxtract-fix-segfault.patch,
        30_fix-excessive-sync.patch and 40_fix_png_header_bytes.patch.
  * debian/README.Debian: added to provide a new tip for users.
  * debian/README.source: the notice in this file is (now) obsolete. Removed.
  * debian/watch: improved.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 25 Oct 2015 17:24:26 -0200

tcpxtract (1.0.1-8) unstable; urgency=low

  * Fix incorrect PNG header magic values (Closes: #649249).

 -- Nico Golde <nion@debian.org>  Sat, 19 Nov 2011 12:51:31 +0100

tcpxtract (1.0.1-7) unstable; urgency=low

  * Fix excessive sync by only fsync'ing the specific output
    file (Closes: #623140).
  * Bump policy version, no changes needed.

 -- Nico Golde <nion@debian.org>  Tue, 19 Apr 2011 16:10:43 +0200

tcpxtract (1.0.1-6) unstable; urgency=low

  * Bump policy version, no changes needed.
  * Switch from dpatch to quilt.
    - Adapted README.source.
  * Switch to dpkg-source 3.0 (quilt) format
  * Add patch by Chow Loong Jin to prevent segfaults due to missing
    null initializations (Closes: #599746).

 -- Nico Golde <nion@debian.org>  Sun, 17 Oct 2010 18:26:09 +0200

tcpxtract (1.0.1-5) unstable; urgency=low

  * Bump policy version, no changes needed.
  * Remove notice about info page from manpage (Closes: #549219).
  * Bump compat level to 7 and adjust debhelper dependency.
  * Version debian packaging license -> GPL-2.
  * Add patch descriptions.
  * dh_clean -l => dh_prep (dh_clean -k deprecated).
  * Add README.source dummy ;)

 -- Nico Golde <nion@debian.org>  Sun, 17 Oct 2010 18:07:01 +0200

tcpxtract (1.0.1-4) unstable; urgency=low

  * Switched from Homepage tag to the new Homepage control field.
  * Added dependency on dpatch and added dpatch stuff to rules.
  * Added a short description for the manpage that actually
    makes sense (01_fixmanpage.dpatch).
  * Fixed debian-rules-ignores-make-clean-error lintian warning.
  * Added AUTHORS to the installed files in doc.

 -- Nico Golde <nion@debian.org>  Fri, 16 Nov 2007 13:03:02 +0100

tcpxtract (1.0.1-3) unstable; urgency=low

  * Added watch file.
  * Fixed copyright year in copyright file.
  * Removed config.guess file from diff.

 -- Nico Golde <nion@debian.org>  Thu, 03 May 2007 18:59:54 +0200

tcpxtract (1.0.1-2) unstable; urgency=low

  * Changed maintainer address.
  * Fixed broken copyright file.
  * Bumped compat version to 5.
  * Bumped standards version, no changes needed.
  * Removed unneeded comments from rules.

 -- Nico Golde <nion@debian.org>  Sat, 17 Mar 2007 16:58:44 +0100

tcpxtract (1.0.1-1) unstable; urgency=low

  * Initial release (Closes: #333282).

 -- Nico Golde <nico@ngolde.de>  Sun, 16 Oct 2005 19:49:29 +0200
